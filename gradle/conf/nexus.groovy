environments {
    linodeNexus {
        baseUrl = "https://nexus3.fhoster.com/repository"
        releaseUrl = "$baseUrl/cloudlet-spi-annotation-processor/"
        snapshotUrl = "$baseUrl/cloudlet-spi-annotation-processor-snapshot/"
        url = "$baseUrl/maven-public/"
    }    
}
