# Cloudlet SPI Annotation Processor #

This tool aids the development of a Livebase Cloudlet Plugin by providing a way to automatically configure the OSGi files needed for running the Plugin inside its Cloudlet.

- Include this library in your project classpath and enable the annotation processor (Gradle configuration below)
- Simply annotate your Handler implementations with its corresponding annotation.
- The annotation processor will automatically generate an OSGI-compliant blueprint.
- Blueprints will be included in the Plugin's bundle.

## Annotations
Available annotations are:

- __@CloudletEventHandler__, for classes implementing any model-dependent Handler SPI, such as _SPI..FormActionHandler_, _SPI..DBInsertHandler_, etc.;
- __@CloudletAuthenticationHandler__, for classes implementing _SPICloudletAuthenticationHandler_;
- __@CloudletLogoutHandler__, for classes implementing _SPICloudletLogoutHandler_;
- __@CloudletScheduledTask__, for classes implementing _SPICloudletScheduledTask_.
    - This annotation also provides the attribute __String defaultCronExpression__ for configuring the CRON job. See https://en.wikipedia.org/wiki/Cron for syntax reference. 

## Setup
All [Cloudlet Plugin Templates](https://bitbucket.org/fhostercom/cloudlet-plugin-templates) comes preconfigured for running the Annotation Processor by command line and in Eclipse. 
If you are not using the templates, simply include the following snippets in your project's `build.gradle` (__compatibility: Gradle 4.7+__):

```
#!groovy
repositories {
  jcenter()
}

def processorLib = 'com.fhoster.livebase:cloudlet-spi-annotation-processor:+' //use latest version
dependencies {
  compileOnly processorLib         //required to resolve annotations at compile time
  annotationProcessor processorLib //for processing annotations
}

def blueprintsDir = "generated"
sourceSets.main.output.resourcesDir = blueprintsDir
compileJava.options.annotationProcessorGeneratedSourcesDirectory = file("${projectDir}/${blueprintsDir}")

clean.doLast { project.delete(blueprintsDir) }
```

## Usage examples
Simply add the annotation to your classes:
```
#!java
import com.fhoster.livebase.CloudletEventHandler;
import com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerHandler1;

@CloudletEventHandler
public class MyFormImpl implements SpiClass1FormActionHandlerHandler1 {
  ...
}
```
For Scheduled tasks, you have to specify the CRON expression. For example, to execute the task every minute:
```
import com.fhoster.livebase.CloudletScheduledTask;
import com.fhoster.livebase.cloudlet.SpiCloudletScheduledTask;

#!java
@CloudletScheduledTask(defaultCronExpression = "* * * * *")
public class MyScheduledTask implements SpiCloudletScheduledTask {
  ...
}
```

## Running the Annotation Processor
Run `gradle build` from a shell in your project's root. The annotation processor will generate a blueprint for every annotated class in your project.
For class `MyHandlerImpl`, corresponding blueprint will be `plugin.MyHandlerImpl.blueprint.xml`.

By default, the blueprints will be available at `/projectRoot/generated/<OSGI-INF>/<blueprint>/<your_blueprints>.xml`. 
You can edit`blueprintsDir` from the snippet above to change this path.

When the build has completed, your jar will include all the blueprints at path `/OSGI-INF/blueprint/`.

The blueprint directory will be also deleted when running `gradle clean`.


## Using the Annotation Processor with Eclipse
If you use Eclipse, also add the following snippet to your project's `build.gradle` to enable real time annotation processing:

```
#!groovy
plugins {
  id 'eclipse'
  id  'net.ltgt.apt-eclipse' version '0.21'
}

project.eclipse.jdt.apt.aptEnabled = true
project.eclipse.jdt.apt.reconcileEnabled = true
project.eclipse.jdt.apt.genSrcDir = blueprintsDir
}
```
Run `gradle eclipse` from a shell in your project's root, then import the project in Eclipse, right click on the project and select `gradle > refresh gradle project`.
`blueprintsDir` will be included in the project's classpath. 


## Using annotations with injectable services

The generated blueprint also include every Cloudlet Framework Service declared in the constructor of the annotated class. 
Declared services will be injected by the framework during instantiation of the plugin.

Available services are: _CloudletIdGenerator_, _CloudletDataSource_, _CloudletMailSystem_, _CloudletMemberManager_, _CloudletSessionFactory_ (see the links below for further info).

For example, the following class:

```
#!java
import com.fhoster.livebase.CloudletEventHandler;
import com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerHandler1;
import com.fhoster.livebase.cloudlet.CloudletIdGenerator;

@CloudletEventHandler
public class MyFormImpl implements SpiClass1FormActionHandlerHandler1 {
  private CloudletIdGenerator idgen;
  
  public MyFormImpl(CloudletIdGenerator idgen) {
    this.idgen = idgen
  }
  ...
}
```

Corresponds to the following blueprint:
```
#!xml
<?xml version="1.0" encoding="UTF-8"?>
	<blueprint xmlns="http://www.osgi.org/xmlns/blueprint/v1.0.0"
	xmlns:ext="http://aries.apache.org/blueprint/xmlns/blueprint-ext/v1.0.0">
	
	<reference id="idGenerator1"
		interface="com.fhoster.livebase.cloudlet.CloudletIdGenerator"
		ext:proxy-method="classes" />                                
						
	<bean id="bid.plugin.myformimpl" class="plugin.MyFormImpl">
		<argument  ref="idGenerator1"/>
	</bean>
	
	<service ref="bid.plugin.myformimpl" id="sid.plugin.myformimpl"
		interface="com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerHandler4">
	</service>
		
</blueprint>
```

## Changelog
- _1.0.62_ : Added support for CloudletSessionFactory dependency injection.
- _1.0.58_ : Added @CloudletLogoutHandler annotation.
- _1.0.56_ : Only one uber jar. Jar with ide classifier no longer exists.

## Further readings
Please refer to the [Livebase Docs](https://docs.livebase.com/guide/plugins/intro/) for further information (at the moment, only in Italian)
To see your Cloudlet's SPI javadoc, append `docs/index.html` to your Cloudlet's URL.

## Useful links
- [The OSGi Alliance](http://osgi.org/)
- [Apache Aries](http://aries.apache.org/)