/**
 *   Copyright 2021 Fhoster srl
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase

import java.util.Set
import javax.annotation.processing.AbstractProcessor
import javax.annotation.processing.ProcessingEnvironment
import javax.annotation.processing.RoundEnvironment
import javax.lang.model.SourceVersion
import javax.lang.model.element.Element
import javax.lang.model.element.ElementKind
import javax.lang.model.element.ExecutableElement
import javax.lang.model.element.TypeElement
import javax.lang.model.type.DeclaredType
import javax.tools.Diagnostic
import javax.tools.StandardLocation
import java.lang.annotation.Annotation
import javax.lang.model.element.Modifier

abstract class AnnotationProcessor extends AbstractProcessor {

	ProcessingEnvironment processingEnvironment

	var _printer = [String message|processingEnvironment.messager.printMessage(Diagnostic.Kind.NOTE, message)]

	override synchronized init(ProcessingEnvironment processingEnv) {
		this.processingEnvironment = processingEnv
	}

	def void setPrinter((String)=>void p) {
		this._printer = p
	}

	override process(Set<? extends TypeElement> annotations, RoundEnvironment env) {
		try {
			for (TypeElement te : annotations) {
				for (Element e : env.getElementsAnnotatedWith(te)) {
		            val annotation = e.getAnnotation(supportedAnnotation)
                    if (annotation!==null) {
    					processAnnotatedClass(e)
                    }
				}
			}
		} catch(InvalidPluginConfigurationException e) {
			return true
		}
		return true
	}

	abstract def void processAnnotatedClass(Element element);

	protected def String[] extractConstructorTypes(ExecutableElement constructor) {
	   constructor.parameters.map[p|p.asType.toString]
	}

	protected def check(Element e, ()=>boolean condition) {
		new CheckElse(e, condition, this)
	}

	protected def check(Element e, ()=>boolean condition, String message) {
		if(!condition.apply) {
			e.error(message)
			throw new InvalidPluginConfigurationException
		}
	}

	protected def error(Element e, String msg) {
		processingEnvironment.messager.printMessage(Diagnostic.Kind.ERROR, msg, e);
	}

	protected def warning(Element e, String msg) {
		processingEnvironment.messager.printMessage(Diagnostic.Kind.WARNING, msg, e);
	}

	override getSupportedAnnotationTypes() {
		val annotations = newHashSet
		annotations.add(supportedAnnotation.canonicalName)
		annotations
	}

	abstract def Class<? extends Annotation> supportedAnnotation()

	override getSupportedSourceVersion() {
		SourceVersion.latestSupported
	}

	protected def info(String m) {
		_printer.apply(m);
	}

	protected def extractPackage(TypeElement elementInterface) {
		processingEnvironment.elementUtils.getPackageOf(elementInterface)
	}

	protected def createResource(StandardLocation location, String fileName) {
		processingEnvironment.filer.createResource(location, "", fileName)
	}

	private def constructors(Element e) {
		(e as TypeElement).enclosedElements.filter[kind == ElementKind.CONSTRUCTOR].map [
			it as ExecutableElement
		]
	}
	
	private def publicConstructors(Element e) {
		(e as TypeElement).enclosedElements.filter[kind == ElementKind.CONSTRUCTOR && modifiers.contains(Modifier.PUBLIC)].map [
			it as ExecutableElement
		]
	}
	
	protected def inectableConstructors(Element e) {
		val res = constructors(e);
		if (res.size() <= 1) {
			return res
		}
		val pubRes = publicConstructors(e);
		if (pubRes.size() == 0) {
			return res
		}
		return pubRes
	}
	
	protected def inectableConstructor(Element e) {
		var res = inectableConstructors(e);
		if (res.size() == 1) {
			return res.head
		}
		throw new InvalidPluginConfigurationException
	}

	protected def interfaces(Element e) {
		(e as TypeElement).interfaces.map [
			((it as DeclaredType).asElement as TypeElement
			)
		        ]
	}
	
	protected def superclass(Element e) {
		return (e as TypeElement).superclass
	}

	protected def isClass(Element e) {
		e.kind == ElementKind.CLASS
	}
	
	protected def isPublicClass(Element e) {
		e.kind == ElementKind.CLASS && e.modifiers.contains(Modifier.PUBLIC)
	}
}

class CheckElse {

	Element e

	()=>boolean c

	AnnotationProcessor p

	new(Element e, ()=>boolean c, AnnotationProcessor p) {
		this.e = e
		this.c = c
		this.p = p
	}

	def orError(String msg) {
		p.check(e, c, msg)
	}
}

class InvalidPluginConfigurationException extends Throwable {
}

