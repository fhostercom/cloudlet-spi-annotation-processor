/**
 *   Copyright 2021 Fhoster srl
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase

import com.fhoster.blueprintassisted.BlpServiceReference
import java.io.PrintWriter
import java.util.Collection
import javax.lang.model.element.Element
import javax.lang.model.element.ExecutableElement
import javax.tools.StandardLocation
import java.util.List

abstract class BlueprintAnnotationProcessor extends AnnotationProcessor {

    UniqueIdGenerator generator
    String uniqueId
    
    new(UniqueIdGenerator generator) {
        this.generator = generator
    }
    
	protected def generateBlueprintXML(Element c, String serviceProperties) {
		val annot = c.getAnnotation(supportedAnnotation)
		val className = c.toString
		val beanId = c.beanID
		val serviceId = serviceID(className)
		val onlyConstructor = c.inectableConstructor
		var String elementInterface = null
		if(c.interfaces.empty) {
			elementInterface = c.superclass.toString
		} else{
			elementInterface = c.interfaces.head.toString
		}

		var fileName = '''OSGI-INF/blueprint/«className».blueprint.xml'''

		val hasDraft = annot.annotationType.declaredMethods.filter[name == "draft"]
		if(hasDraft.size > 0) {
			val draft = hasDraft.head.invoke(annot) as Boolean
			if(draft)
				fileName += ".draft"
		}

		val location = StandardLocation.SOURCE_OUTPUT
		val file = createResource(location, fileName)

		uniqueId = generator.getUniqueId()
		
        val predefinedServiceReferences = newArrayList
        predefinedServiceReferences.addAll(onlyConstructor.assistedBlueprintReferences)
        predefinedServiceReferences.addAll(knownBlueprintReferences)
        
		val constructorArgs = processElementConstructor(onlyConstructor, predefinedServiceReferences)
		val out = new PrintWriter(file.openOutputStream)
		val part = '''
			<?xml version="1.0" encoding="UTF-8"?>
				<blueprint xmlns="http://www.osgi.org/xmlns/blueprint/v1.0.0"
				xmlns:ext="http://aries.apache.org/blueprint/xmlns/blueprint-ext/v1.0.0">
				
				«predefinedServiceReferences.filter[it.used].map[it.xmlSnippet].join('\n')»
									
				<bean id="«beanId»" class="«className»" scope="singleton">
					«constructorArgs.join('\n')»
				</bean>
				
			
				<service ref="«beanId»" id="«serviceId»"
					interface="«elementInterface»">
					«serviceProperties»
				</service>
					
				</blueprint>
			'''
		out.write(part)
		out.close
	}
    
    def Collection<? extends BlueprintReference> getAssistedBlueprintReferences(ExecutableElement onlyConstructor) {
        onlyConstructor.parameters
            .filter[it.getAnnotation(BlpServiceReference)!==null]
            .map[annotated|
                val annotation=annotated.getAnnotation(BlpServiceReference)
                var refId = if (annotation.id != "") annotation.id else '''assistedReference«uniqueId»'''
                
                new BlueprintReference(annotated.asType.toString,
                '''<argument ref="«refId»"/>''', 
                '''
                <reference id="«refId»"
                    interface="«annotated.asType.toString»"
                    «IF annotation.filter != ""»
                        filter="«annotation.filter»"
                    «ENDIF»
                    />
                ''',
                annotated.annotationMirrors.map[toString].toList 
                )].toList
    }
    
	protected def serviceID(String className) {
		'''sid.«className.toLowerCase»'''
	}

	protected def beanID(Element c) {
		'''bid.«c.toString.toLowerCase»'''
	}

	private def getKnownBlueprintReferences() {
		#[
			new BlueprintReference("org.osgi.framework.BundleContext", 
			'''<argument  ref="blueprintBundleContext"/>''', ''''''),
			new BlueprintReference("com.fhoster.livebase.cloudlet.CloudletDataSource", 
			'''<argument  ref="dataSource«uniqueId»"/>''','''
				<reference id="dataSource«uniqueId»"
					interface="com.fhoster.livebase.cloudlet.CloudletDataSource"
					ext:proxy-method="classes" />
			'''),
			new BlueprintReference("javax.sql.DataSource", 
			'''<argument  ref="sqlDataSource«uniqueId»"/>''', '''
				<reference id="sqlDataSource«uniqueId»"
					interface="com.fhoster.livebase.cloudlet.CloudletDataSource"
					ext:proxy-method="classes" />
			'''),
			new BlueprintReference("com.fhoster.livebase.cloudlet.CloudletMailSystem", 
			'''<argument  ref="mailSystem«uniqueId»"/>''','''
				<reference id="mailSystem«uniqueId»"
					interface="com.fhoster.livebase.cloudlet.CloudletMailSystem"
					ext:proxy-method="classes" />
			'''),
			new BlueprintReference("com.fhoster.livebase.cloudlet.CloudletMemberManager", 
			'''<argument  ref="memberManager«uniqueId»"/>''','''
				<reference id="memberManager«uniqueId»"
					interface="com.fhoster.livebase.cloudlet.CloudletMemberManager"
					ext:proxy-method="classes" />
			'''),
			new BlueprintReference("com.fhoster.livebase.cloudlet.CloudletIdGenerator", 
			'''<argument  ref="idGenerator«uniqueId»"/>''', '''
				<reference id="idGenerator«uniqueId»"
					interface="com.fhoster.livebase.cloudlet.CloudletIdGenerator"
					ext:proxy-method="classes" />
			'''),
			new BlueprintReference("com.fhoster.livebase.cloudlet.EntitySessionFactory", 
			'''<argument  ref="entitySessionFactory«uniqueId»"/>''', '''
				<reference id="entitySessionFactory«uniqueId»"
					interface="com.fhoster.livebase.cloudlet.EntitySessionFactory"
					ext:proxy-method="classes" />
			'''),
			new BlueprintReference("com.fhoster.livebase.cloudlet.CloudletEntitySessionFactory", 
			'''<argument  ref="cloudletEntitySessionFactory«uniqueId»"/>''', '''
				<reference id="cloudletEntitySessionFactory«uniqueId»"
					interface="com.fhoster.livebase.cloudlet.CloudletEntitySessionFactory"
					ext:proxy-method="classes" />
			'''),
			new BlueprintReference("com.fhoster.livebase.cloudlet.CloudletSessionFactory", 
			'''<argument  ref="sessionFactory«uniqueId»"/>''', '''
				<reference id="sessionFactory«uniqueId»"
					interface="com.fhoster.livebase.cloudlet.CloudletSessionFactory"
					ext:proxy-method="classes" />
			'''),
			new BlueprintReference("com.fhoster.livebase.cloudlet.EntityLockManager", 
			'''<argument  ref="entityLockManager«uniqueId»"/>''', '''
				<reference id="entityLockManager«uniqueId»"
					interface="com.fhoster.livebase.cloudlet.EntityLockManager"
					ext:proxy-method="classes" />
			'''),
			new BlueprintReference("com.fhoster.livebase.cloudlet.SpiCloudletSMSSender", 
			'''<argument  ref="smssender«uniqueId»"/>''', '''
				<reference id="smssender«uniqueId»"
					interface="com.fhoster.livebase.cloudlet.SpiCloudletSMSSender"
					ext:proxy-method="classes" />
			'''),
			new BlueprintReference("com.fhoster.livebase.cloudlet.__CurrentUserLoader",
            '''<argument  ref="currentuserloader«uniqueId»"/>''', '''
                <reference id="currentuserloader«uniqueId»"
                    interface="com.fhoster.livebase.cloudlet.__CurrentUserLoader"
                    ext:proxy-method="classes" />
            ''')
		]
	}

	private def processElementConstructor(ExecutableElement constructor,
		Iterable<BlueprintReference> references) {
		val constructorArgs = newArrayList()
		
		for (t : constructor.parameters) {
			val reference = references.findFirst[ br |
			    if (br.name != t.asType.toString) return false
			    
			    val filter = t.annotationMirrors.map[toString].findFirst[am | br.filters.contains(am)]
			    if (filter !== null) return true
			    
			    //to manage known types without annotation
			    if (br.filters.empty) return true
			    
    		    return false
			]
			if(reference !== null) {
				reference.used = true
				constructorArgs.add(reference.argumentSnippet)
			} else {
				constructorArgs.add('''<argument value=".. type here your value ..." />''')
			}
		}
		constructorArgs
	}

}

class BlueprintReference {
	public String name
	public String xmlSnippet
	public String argumentSnippet
	public List<String> filters
	public boolean used

	new(String name, String argumentSnippet, String xmlSnippet) {
		this(name, argumentSnippet, xmlSnippet, #[])
	}
	
	new(String name, String argumentSnippet, String xmlSnippet, List<String> filters) {
        this.name = name
        this.argumentSnippet = argumentSnippet
        this.xmlSnippet = xmlSnippet
        this.filters = filters
    }
}