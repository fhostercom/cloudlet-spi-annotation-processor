/**
 *   Copyright 2021 Fhoster srl
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase

import javax.annotation.processing.SupportedAnnotationTypes
import javax.lang.model.element.Element

@SupportedAnnotationTypes("com.fhoster.livebase.CloudletSMSSender")
class CloudletSMSSenderAnnotationProcessor extends BlueprintAnnotationProcessor {

    new(UniqueIdGenerator generator) {
        super(generator)
    }
    
    override processAnnotatedClass(Element e) {
		if(hasErrors(e))
			return 
		else
			generateBlueprintXML(e, "")
	}

	private def hasErrors(Element e) {
		e.check[e.isClass].orError('''The @CloudletSMSSender annotation can only be used on classes, not interfaces  ''')
		e.check[e.isPublicClass].orError('''The @CloudletSMSSender annotation can only be used on public classes.''')

		if(e.inectableConstructors.size > 0) {
			e.check[e.inectableConstructors.size == 1].orError('''Too many constructors found. A plugin must have at most one costructor or at most one public constructor''')
		}

		if(e.interfaces.size > 1) {
			e.error("This class must implement one interface only. Refer to the Plugin docs for more information.")
			return true;
		}
		return false;
	}

	override supportedAnnotation() {
		CloudletSMSSender
	}

}