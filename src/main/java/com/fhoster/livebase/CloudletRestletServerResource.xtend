/**
 *   Copyright 2021 Fhoster srl
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase

import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target

/**
 * This annotation is used when implementing a RestletServerResource.
 * <br>
 * When used in combination with gradle and the CloudletRestletServerResourceAnnotationProcessor,
 * the handler configuration file (the blueprint) will be automatically generated.
 * <br>
 * Configuration errors will be shown in the editor at compile time.   
 * <br>
 * <br>
 * You can optionally give an id for this plugin:
 * <br>
 * ex: <b> @CloudletRestletServerResource("myPlugin")</b>
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
annotation CloudletRestletServerResource {

	/**
	 * Used to give a custom id to the handler (optional)
	 * @return "" as default
	 */
	String value = ""

	/**
	 * Used to generate a draft blueprint file that will be ignored at runtime
	 * @return false as default
	 */
	boolean draft = false
}





