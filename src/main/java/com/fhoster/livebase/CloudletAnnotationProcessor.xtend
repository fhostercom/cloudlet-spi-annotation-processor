/**
 *   Copyright 2021 Fhoster srl
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase

import javax.annotation.processing.AbstractProcessor
import java.util.Set
import javax.lang.model.element.TypeElement
import javax.annotation.processing.RoundEnvironment
import javax.annotation.processing.ProcessingEnvironment
import javax.lang.model.element.Element
import javax.lang.model.element.AnnotationMirror
import javax.lang.model.element.ExecutableElement
import java.util.List

class CloudletAnnotationProcessor extends AbstractProcessor {
    
    val List<AbstractProcessor> processors
    
    new() {
        val uniqueId = new SequentialUniqueIdGenerator
        
        processors = #[
            new CloudletEventHandlerAnnotationProcessor(uniqueId), 
            new CloudletScheduledTaskAnnotationProcessor(uniqueId),
            new CloudletAuthenticationHandlerAnnotationProcessor(uniqueId),
            new CloudletLogoutHandlerAnnotationProcessor(uniqueId),
            new CloudletRestletServerResourceAnnotationProcessor(uniqueId),
            new CloudletSMSSenderAnnotationProcessor(uniqueId),
            new CloudletPluginActivatorAnnotationProcessor(uniqueId)
            ]
    }
    override synchronized init(ProcessingEnvironment processingEnv) {
        processors.forEach[p|p.init(processingEnv)]
    }
    
    override process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        processors.map[p|p.process(annotations,roundEnv)].forall[it]
    }
    
    override getSupportedAnnotationTypes() {
        processors.map[ p | p.supportedAnnotationTypes].flatten.toSet
    }
    
    override getCompletions(Element element, AnnotationMirror annotation, ExecutableElement member, String userText) {
        processors.map[ p | p.getCompletions(element, annotation, member, userText)].flatten
    }
    
    override getSupportedOptions() {
         processors.map[ p | p.supportedOptions].flatten.toSet
    }
    
    override getSupportedSourceVersion() {
        processors.head.supportedSourceVersion
    }
    
}