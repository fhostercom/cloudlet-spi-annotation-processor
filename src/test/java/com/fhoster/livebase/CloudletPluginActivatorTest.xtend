/**
 *   Copyright 2021 Fhoster srl
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase

import org.junit.Test

import static org.hamcrest.CoreMatchers.*
import static org.junit.Assert.*
import static org.xmlmatchers.XmlMatchers.*
import static org.xmlmatchers.transform.XmlConverters.the
import org.junit.Before

class CloudletPluginActivatorTest extends TestProcessor{
	
    CloudletPluginActivatorAnnotationProcessor sut
    
    @Before
    def void setup() {
        sut = new CloudletPluginActivatorAnnotationProcessor(new SequentialUniqueIdGenerator)
    }
    
	@Test
	def void verifyBlueprintGeneration() {

		var class1 = getSourceFromCode("MyPluginActivator", '''
			import org.osgi.framework.BundleContext;
			import com.fhoster.livebase.CloudletPluginActivator;
			import com.fhoster.livebase.cloudlet.SpiCloudletPluginActivator;
			import com.fhoster.livebase.cloudlet.CloudletDataSource;
			
			@CloudletPluginActivator
			public class MyPluginActivator implements SpiCloudletPluginActivator {
				
				public MyPluginActivator(BundleContext b, CloudletDataSource x){}
			
				@Override
				public void start() {
					// do nothing
				}
				
				@Override
				public void stop() {
					// do nothing
				}
			}
		''')

		val outputs = sut.processClass(class1)
		val out = outputs.get('MyPluginActivator.blueprint.xml')
		println(out)

		assertThat(the(out), is(equivalentTo(
			the('''
				<blueprint xmlns:ext="http://aries.apache.org/blueprint/xmlns/blueprint-ext/v1.0.0">
					<reference id="dataSource1"
					      interface="com.fhoster.livebase.cloudlet.CloudletDataSource"
					      ext:proxy-method="classes" />
										
					<bean id="bid.mypluginactivator" class="MyPluginActivator" scope="singleton">
					    <argument ref="blueprintBundleContext"/>
						<argument ref="dataSource1"/>
					</bean>
					
					<service 
						ref="bid.mypluginactivator" 
						id="sid.mypluginactivator"
						interface="com.fhoster.livebase.cloudlet.SpiCloudletPluginActivator">
					</service>
						
				</blueprint>''')
		)));

	}
	
}