/**
 *   Copyright 2021 Fhoster srl
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase

import org.junit.Test

import static org.hamcrest.CoreMatchers.*
import static org.junit.Assert.*
import static org.xmlmatchers.XmlMatchers.*
import static org.xmlmatchers.transform.XmlConverters.the
import org.junit.Before

class CloudletEventHandlerTest extends TestProcessor {

    CloudletEventHandlerAnnotationProcessor sut
    
    @Before
    def void setup() {
        sut = new CloudletEventHandlerAnnotationProcessor(new SequentialUniqueIdGenerator)   
    }
    
	@Test
	def void verifyPartWasGeneratedOnFormActionPlugin() {

		var classes = getSourceFromCode("MyFormActionPlugin", '''
			import com.fhoster.livebase.cloudlet.Class1FormActionContext;
			import com.fhoster.livebase.cloudlet.HandlerException;
			import com.fhoster.livebase.cloudlet.HandlerResult;
			import com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1;
			import com.fhoster.livebase.CloudletEventHandler;
			
			@CloudletEventHandler
			public class MyFormActionPlugin implements SpiClass1FormActionHandlerMyPlugin1 {
			
			  @Override
			  public HandlerResult doAction(Class1FormActionContext arg0) throws HandlerException {
			    return null;
			  }
			}
		''')

		val outputs = sut.processClass(classes)
		val out = outputs.get('MyFormActionPlugin.blueprint.xml')
		println(out)

		assertThat(the(out), is(equivalentTo(
			the('''
			<blueprint xmlns:ext="http://aries.apache.org/blueprint/xmlns/blueprint-ext/v1.0.0">
				<bean 
				     id="bid.myformactionplugin" 
				     class="MyFormActionPlugin"
				     scope="singleton">
				</bean>
				
				<service 
				     ref="bid.myformactionplugin" 
				     id="sid.myformactionplugin" 
				     interface="com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1">
				</service>
			</blueprint>''')
		)))
	}

	@Test
	def void verifyClassWithPackage() {

		var classes = getSourceFromCode("asdasd.MyFormActionPlugin", '''
			package asdasd;
			import com.fhoster.livebase.cloudlet.Class1FormActionContext;
			import com.fhoster.livebase.cloudlet.HandlerException;
			import com.fhoster.livebase.cloudlet.HandlerResult;
			import com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1;
			import com.fhoster.livebase.CloudletEventHandler;
			
			@CloudletEventHandler
			public class MyFormActionPlugin implements SpiClass1FormActionHandlerMyPlugin1 {
			
			  @Override
			  public HandlerResult doAction(Class1FormActionContext arg0) throws HandlerException {
			    return null;
			  }
			}
		''')

		val outputs = sut.processClass(classes)
		val out = outputs.get('asdasd.MyFormActionPlugin.blueprint.xml')
		println(out)

		assertThat(the(out), is(equivalentTo(
			the('''
			<blueprint xmlns:ext="http://aries.apache.org/blueprint/xmlns/blueprint-ext/v1.0.0">
			    <bean 
			         id="bid.asdasd.myformactionplugin" 
			         class="asdasd.MyFormActionPlugin"
			         scope="singleton">
			    </bean>
			    
			    <service 
			         ref="bid.asdasd.myformactionplugin" 
			         id="sid.asdasd.myformactionplugin" 
			         interface="com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1">
			    </service>
			</blueprint>''')
		)))
	}

	@Test
	def void verifyPartWasGeneratedOnFormActionPluginWithCloudlet() {

		var classes = getSourceFromCode("MyFormActionPlugin2", '''
			import com.fhoster.livebase.cloudlet.Class1FormActionContext;
			import com.fhoster.livebase.cloudlet.CloudletDataSource;
			import com.fhoster.livebase.cloudlet.HandlerException;
			import com.fhoster.livebase.cloudlet.HandlerResult;
			import com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1;
			import com.fhoster.livebase.CloudletEventHandler;
			
			@CloudletEventHandler
			public class MyFormActionPlugin2 implements SpiClass1FormActionHandlerMyPlugin1 {
				
				public MyFormActionPlugin2(CloudletDataSource x){
					
				}
			
			  @Override
			  public HandlerResult doAction(Class1FormActionContext context) throws HandlerException {
			    return context.result().withMessage("ciao").info();
			  }
			}
		''')

		val outputs = sut.processClass(classes)
		val out = outputs.get('MyFormActionPlugin2.blueprint.xml')
		println(out)

		assertThat(the(out), is(equivalentTo(the('''
		<blueprint xmlns:ext="http://aries.apache.org/blueprint/xmlns/blueprint-ext/v1.0.0">
		<reference 
		      id="dataSource1"
		      interface="com.fhoster.livebase.cloudlet.CloudletDataSource"
		      ext:proxy-method="classes" />
		      
			<bean 
			     id="bid.myformactionplugin2" 
			     class="MyFormActionPlugin2"
			     scope="singleton">
			     
				<argument ref="dataSource1"/>
			</bean>
			
			<service 
			     ref="bid.myformactionplugin2" 
			     id="sid.myformactionplugin2" 
			     interface="com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1">
			</service>
		</blueprint>'''))))

	}

	@Test
	def void verifyPartWasGeneratedOnFormActionPluginWithCloudletDataSource() {

		var classes = getSourceFromCode("MyFormActionPlugin2", '''
			import com.fhoster.livebase.cloudlet.Class1FormActionContext;
			import com.fhoster.livebase.cloudlet.CloudletDataSource;
			import com.fhoster.livebase.cloudlet.HandlerException;
			import com.fhoster.livebase.cloudlet.HandlerResult;
			import com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1;
			import com.fhoster.livebase.CloudletEventHandler;
			
			@CloudletEventHandler
			public class MyFormActionPlugin2 implements SpiClass1FormActionHandlerMyPlugin1 {
			    
			    public MyFormActionPlugin2(CloudletDataSource x){
			        
			    }
			
			  @Override
			  public HandlerResult doAction(Class1FormActionContext context) throws HandlerException {
			    return context.result().withMessage("ciao").info();
			  }
			}
		''')

		val outputs = sut.processClass(classes)
		val out = outputs.get('MyFormActionPlugin2.blueprint.xml')
		println(out)

		assertThat(the(out), is(equivalentTo(
			the('''
			<blueprint xmlns:ext="http://aries.apache.org/blueprint/xmlns/blueprint-ext/v1.0.0">
			<reference id="dataSource1"
			      interface="com.fhoster.livebase.cloudlet.CloudletDataSource"
			      ext:proxy-method="classes" />
			
			    <bean 
			         id="bid.myformactionplugin2" 
			         class="MyFormActionPlugin2"
			         scope="singleton">
			         
			        <argument ref="dataSource1"/>
			    </bean>
			    
			    <service 
			         ref="bid.myformactionplugin2" 
			         id="sid.myformactionplugin2" 
			         interface="com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1">
			    </service>
			</blueprint>''')
		)))

	}

	@Test
	def void verifyPartWasGeneratedOnFormActionPluginWithAdditionalConstructorParams() {

		var classes = getSourceFromCode("MyFormActionPlugin2", '''
			import com.fhoster.livebase.cloudlet.Class1FormActionContext;
			import com.fhoster.livebase.cloudlet.CloudletDataSource;
			import com.fhoster.livebase.cloudlet.HandlerException;
			import com.fhoster.livebase.cloudlet.HandlerResult;
			import com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1;
			import com.fhoster.livebase.CloudletEventHandler;
			
			@CloudletEventHandler
			public class MyFormActionPlugin2 implements SpiClass1FormActionHandlerMyPlugin1 {
			    
			    public MyFormActionPlugin2(CloudletDataSource x, String str){
			        
			    }
			
			  @Override
			  public HandlerResult doAction(Class1FormActionContext context) throws HandlerException {
			    return context.result().withMessage("ciao").info();
			  }
			}
		''')

		val outputs = sut.processClass(classes)
		val out = outputs.get('MyFormActionPlugin2.blueprint.xml')
		println(out)

		assertThat(the(out), is(equivalentTo(
			the('''
			<blueprint xmlns:ext="http://aries.apache.org/blueprint/xmlns/blueprint-ext/v1.0.0">
			<reference id="dataSource1"
			      interface="com.fhoster.livebase.cloudlet.CloudletDataSource"
			      ext:proxy-method="classes" />
			
			    <bean 
			         id="bid.myformactionplugin2" 
			         class="MyFormActionPlugin2"
			         scope="singleton">
			         
			        <argument ref="dataSource1"/>
			        <argument value=".. type here your value ..." />
			    </bean>
			    
			    <service 
			         ref="bid.myformactionplugin2" 
			         id="sid.myformactionplugin2" 
			         interface="com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1">
			    </service>
			</blueprint>''')
		)))

	}
	
    @Test
    def void verifyPartWasGeneratedOnFormActionPluginWithAdditionalAnnotatedConstructorParams() {

        var classes = getSourceFromCode("MyFormActionPlugin2", '''
            import com.fhoster.livebase.cloudlet.Class1FormActionContext;
            import com.fhoster.livebase.cloudlet.CloudletDataSource;
            import com.fhoster.livebase.cloudlet.HandlerException;
            import com.fhoster.livebase.cloudlet.HandlerResult;
            import com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1;
            import com.fhoster.livebase.CloudletEventHandler;
            import com.fhoster.blueprintassisted.BlpServiceReference;
            import javax.sql.DataSource;
            
            @CloudletEventHandler
            public class MyFormActionPlugin2 implements SpiClass1FormActionHandlerMyPlugin1 {
                
                public MyFormActionPlugin2(@BlpServiceReference(id="myId") DataSource ds1, 
                    @BlpServiceReference(filter="(key=value)") DataSource ds2, 
                    @BlpServiceReference(id="myId2", filter="(key=value)") DataSource ds3,
                    @BlpServiceReference(id="myId") DataSource ds4,
                    DataSource ds5){
                    
                }
            
              @Override
              public HandlerResult doAction(Class1FormActionContext context) throws HandlerException {
                return context.result().withMessage("ciao").info();
              }
            }
        ''')

        val outputs = sut.processClass(classes)
        val out = outputs.get('MyFormActionPlugin2.blueprint.xml')
        println(out)

        assertThat(the(out), is(equivalentTo(
            the('''
            <blueprint xmlns:ext="http://aries.apache.org/blueprint/xmlns/blueprint-ext/v1.0.0">          
                <reference id="myId" interface="javax.sql.DataSource" />
                <reference id="assistedReference1" interface="javax.sql.DataSource" filter="(key=value)" />
                <reference id="myId2" interface="javax.sql.DataSource" filter="(key=value)" />
                <reference id="sqlDataSource1" interface="com.fhoster.livebase.cloudlet.CloudletDataSource" 
                	ext:proxy-method="classes" />
                
                <bean 
                     id="bid.myformactionplugin2" 
                     class="MyFormActionPlugin2"
                     scope="singleton">
                    <argument ref="myId"/>
                    <argument ref="assistedReference1" />
                    <argument ref="myId2" />
                    <argument ref="myId" />
                    <argument ref="sqlDataSource1" />
                </bean>
                
                <service 
                     ref="bid.myformactionplugin2" 
                     id="sid.myformactionplugin2" 
                     interface="com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1">
                </service>
            </blueprint>''')
        )))

    }	

	@Test
	def void verifyPartWasGeneratedOnFormActionPluginWithCloudletId() {

		var classes = getSourceFromCode("MyFormActionPlugin2", '''
			import com.fhoster.livebase.cloudlet.Class1FormActionContext;
			import com.fhoster.livebase.cloudlet.CloudletIdGenerator;
			import com.fhoster.livebase.cloudlet.HandlerException;
			import com.fhoster.livebase.cloudlet.HandlerResult;
			import com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1;
			import com.fhoster.livebase.CloudletEventHandler;
			
			@CloudletEventHandler
			public class MyFormActionPlugin2 implements SpiClass1FormActionHandlerMyPlugin1 {
				
				public MyFormActionPlugin2(CloudletIdGenerator x){
					
				}
			
			  @Override
			  public HandlerResult doAction(Class1FormActionContext context) throws HandlerException {
			    return context.result().withMessage("ciao").info();
			  }
			
			}
		''')

		val outputs = sut.processClass(classes)
		val out = outputs.get('MyFormActionPlugin2.blueprint.xml')
		println(out)

		assertThat(the(out), is(equivalentTo(
			the('''
			<blueprint xmlns:ext="http://aries.apache.org/blueprint/xmlns/blueprint-ext/v1.0.0">
			    <reference id="idGenerator1"
			      interface="com.fhoster.livebase.cloudlet.CloudletIdGenerator"
			      ext:proxy-method="classes" />
			      
				<bean 
				     id="bid.myformactionplugin2" 
				     class="MyFormActionPlugin2"
				     scope="singleton">
				     
					<argument ref="idGenerator1"/>
				</bean>
				
				<service id="sid.myformactionplugin2" ref="bid.myformactionplugin2" interface="com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1">
				</service>
			</blueprint>''')
		)))
	}

	@Test
	def void verifyPartWasGeneratedOnDatabaseHandler() {

		var classes = getSourceFromCode("MyFormActionPlugin", '''
			import com.fhoster.livebase.cloudlet.HandlerResult;
			import com.fhoster.livebase.cloudlet.Class1DatabaseDeleteContext;
			import com.fhoster.livebase.cloudlet.Class1DatabaseDeleteTransactionalContext;
			import com.fhoster.livebase.cloudlet.SpiClass1DatabaseDeleteHandlerMyHandler1;
			import com.fhoster.livebase.CloudletEventHandler;
			
			@CloudletEventHandler
			public class MyFormActionPlugin implements SpiClass1DatabaseDeleteHandlerMyHandler1 {
			
			  @Override
			  public HandlerResult afterCommit(Class1DatabaseDeleteContext context) {
			  	return context.result().none();
			  }
			
			  @Override
			  public HandlerResult beforeCommit(Class1DatabaseDeleteTransactionalContext context) {
			    return context.result().none();
			  }
			}
		''')

		val outputs = sut.processClass(classes)
		val out = outputs.get('MyFormActionPlugin.blueprint.xml')
		println(out)

		assertThat(the(out), is(equivalentTo(
			the('''
			<blueprint>
			    <bean 
			        id="bid.myformactionplugin" 
			        class="MyFormActionPlugin"
			        scope="singleton">
			    </bean>
			    
			    <service 
			        ref="bid.myformactionplugin" 
			        id="sid.myformactionplugin" 
			        interface="com.fhoster.livebase.cloudlet.SpiClass1DatabaseDeleteHandlerMyHandler1">
			    </service>
			</blueprint>''')
		)));
	}
	

	@Test
	def void verifyForMultipleFiles() {

		var class1 = getSourceFromCode("MyFormActionPlugin", '''
			import com.fhoster.livebase.cloudlet.HandlerResult;
			import com.fhoster.livebase.cloudlet.Class1DatabaseDeleteContext;
			import com.fhoster.livebase.cloudlet.Class1DatabaseDeleteTransactionalContext;
			import com.fhoster.livebase.cloudlet.SpiClass1DatabaseDeleteHandlerMyHandler1;
			import com.fhoster.livebase.CloudletEventHandler;
			import com.fhoster.livebase.cloudlet.CloudletIdGenerator;
			
			@CloudletEventHandler
			public class MyFormActionPlugin implements SpiClass1DatabaseDeleteHandlerMyHandler1 {
			
			    public MyFormActionPlugin(CloudletIdGenerator x){
			        
			    }
			    
			  @Override
			  public HandlerResult afterCommit(Class1DatabaseDeleteContext context) {
			    return context.result().none();
			  }
			
			  @Override
			  public HandlerResult beforeCommit(Class1DatabaseDeleteTransactionalContext context) {
			    return context.result().none();
			  }
			}
		''')
		var class2 = getSourceFromCode("MyFormActionPlugin2", '''
			import com.fhoster.livebase.cloudlet.HandlerResult;
			import com.fhoster.livebase.cloudlet.Class1DatabaseDeleteContext;
			import com.fhoster.livebase.cloudlet.Class1DatabaseDeleteTransactionalContext;
			import com.fhoster.livebase.cloudlet.SpiClass1DatabaseDeleteHandlerMyHandler1;
			import com.fhoster.livebase.CloudletEventHandler;
			import com.fhoster.livebase.cloudlet.CloudletIdGenerator;
			
			@CloudletEventHandler
			public class MyFormActionPlugin2 implements SpiClass1DatabaseDeleteHandlerMyHandler1 {
			
			    public MyFormActionPlugin2(CloudletIdGenerator x){
			        
			    }
			
			  @Override
			  public HandlerResult afterCommit(Class1DatabaseDeleteContext context) {
			  	return context.result().none();
			  }
			
			  @Override
			  public HandlerResult beforeCommit(Class1DatabaseDeleteTransactionalContext context) {
			    return context.result().none();
			  }
			}
		''')

		val outputs = sut.processClass(class1, class2)
		val out = outputs.get('MyFormActionPlugin.blueprint.xml')
		println(out)

		assertThat(the(out), is(equivalentTo(
			the('''
			<blueprint xmlns:ext="http://aries.apache.org/blueprint/xmlns/blueprint-ext/v1.0.0">
			    <reference id="idGenerator1"
			      interface="com.fhoster.livebase.cloudlet.CloudletIdGenerator"
			      ext:proxy-method="classes" />
			      
			    <bean 
			        id="bid.myformactionplugin" 
			        class="MyFormActionPlugin"
			        scope="singleton">
			        <argument ref="idGenerator1" />
			    </bean>
			    
			    <service 
			        ref="bid.myformactionplugin" 
			        id="sid.myformactionplugin" 
			        interface="com.fhoster.livebase.cloudlet.SpiClass1DatabaseDeleteHandlerMyHandler1">
			    </service>
			</blueprint>''')
		)));

		val out2 = outputs.get('MyFormActionPlugin2.blueprint.xml')
		println(out2)
		assertThat(the(out2), is(equivalentTo(
			the('''
			<blueprint xmlns:ext="http://aries.apache.org/blueprint/xmlns/blueprint-ext/v1.0.0">
			    <reference id="idGenerator2"
			      interface="com.fhoster.livebase.cloudlet.CloudletIdGenerator"
			      ext:proxy-method="classes" />
			      
			    <bean 
			        id="bid.myformactionplugin2" 
			        class="MyFormActionPlugin2"
			        scope="singleton">
			        <argument ref="idGenerator2" />
			    </bean>
			    
			    <service 
			        ref="bid.myformactionplugin2" 
			        id="sid.myformactionplugin2" 
			        interface="com.fhoster.livebase.cloudlet.SpiClass1DatabaseDeleteHandlerMyHandler1">
			    </service>
			</blueprint>''')
		)));
	}
	
	@Test
	def void verifyMultipleConstructorSupport_onlyOnePublic () {

		var classes = getSourceFromCode("MyFormActionPlugin2", '''
			import com.fhoster.livebase.cloudlet.Class1FormActionContext;
			import com.fhoster.livebase.cloudlet.CloudletIdGenerator;
			import com.fhoster.livebase.cloudlet.HandlerException;
			import com.fhoster.livebase.cloudlet.HandlerResult;
			import com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1;
			import com.fhoster.livebase.CloudletEventHandler;
			
			@CloudletEventHandler
			public class MyFormActionPlugin2 implements SpiClass1FormActionHandlerMyPlugin1 {
				
				public MyFormActionPlugin2(CloudletIdGenerator x){
					
				}
				
				MyFormActionPlugin2(String x){
					
				}
			
			  @Override
			  public HandlerResult doAction(Class1FormActionContext context) throws HandlerException {
			    return context.result().withMessage("ciao").info();
			  }
			
			}
		''')

		val outputs = sut.processClass(classes)
		val out = outputs.get('MyFormActionPlugin2.blueprint.xml')
		println(out)

		assertThat(the(out), is(equivalentTo(
			the('''
			<blueprint xmlns:ext="http://aries.apache.org/blueprint/xmlns/blueprint-ext/v1.0.0">
			    <reference id="idGenerator1"
			      interface="com.fhoster.livebase.cloudlet.CloudletIdGenerator"
			      ext:proxy-method="classes" />
			      
				<bean 
				     id="bid.myformactionplugin2" 
				     class="MyFormActionPlugin2"
				     scope="singleton">
				     
					<argument ref="idGenerator1"/>
				</bean>
				
				<service id="sid.myformactionplugin2" ref="bid.myformactionplugin2" interface="com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1">
				</service>
			</blueprint>''')
		)))
	}
	
	@Test
	def void verifyMultipleConstructorSupport_onlyMutiplePublic () {

		var classes = getSourceFromCode("MyFormActionPlugin2", '''
			import com.fhoster.livebase.cloudlet.Class1FormActionContext;
			import com.fhoster.livebase.cloudlet.CloudletIdGenerator;
			import com.fhoster.livebase.cloudlet.HandlerException;
			import com.fhoster.livebase.cloudlet.HandlerResult;
			import com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1;
			import com.fhoster.livebase.CloudletEventHandler;
			
			@CloudletEventHandler
			public class MyFormActionPlugin2 implements SpiClass1FormActionHandlerMyPlugin1 {
				
				public MyFormActionPlugin2(CloudletIdGenerator x){
					
				}
				
				public MyFormActionPlugin2(String x){
					
				}
			
			  @Override
			  public HandlerResult doAction(Class1FormActionContext context) throws HandlerException {
			    return context.result().withMessage("ciao").info();
			  }
			
			}
		''')

		sut.processClassFailure(classes)
	}
	
	@Test
	def void verifyMultipleConstructorSupport_notPublic () {

		var classes = getSourceFromCode("MyFormActionPlugin2", '''
			import com.fhoster.livebase.cloudlet.Class1FormActionContext;
			import com.fhoster.livebase.cloudlet.CloudletIdGenerator;
			import com.fhoster.livebase.cloudlet.HandlerException;
			import com.fhoster.livebase.cloudlet.HandlerResult;
			import com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1;
			import com.fhoster.livebase.CloudletEventHandler;
			
			@CloudletEventHandler
			public class MyFormActionPlugin2 implements SpiClass1FormActionHandlerMyPlugin1 {
				
				MyFormActionPlugin2(CloudletIdGenerator x){
					
				}
				
				MyFormActionPlugin2(String x){
					
				}
			
			  @Override
			  public HandlerResult doAction(Class1FormActionContext context) throws HandlerException {
			    return context.result().withMessage("ciao").info();
			  }
			
			}
		''')

		sut.processClassFailure(classes)
	}

}