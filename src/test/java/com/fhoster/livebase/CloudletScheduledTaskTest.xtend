/**
 *   Copyright 2021 Fhoster srl
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase

import org.junit.Test

import static org.hamcrest.CoreMatchers.*
import static org.junit.Assert.*
import static org.xmlmatchers.XmlMatchers.*
import static org.xmlmatchers.transform.XmlConverters.the
import org.junit.Before

class CloudletScheduledTaskTest extends TestProcessor{
	
    CloudletScheduledTaskAnnotationProcessor sut
    
    @Before
    def void setup() {
        sut = new CloudletScheduledTaskAnnotationProcessor(new SequentialUniqueIdGenerator)
    }
	
	@Test
	def void verifyForMultipleFiles() {

		var class1 = getSourceFromCode("MyScheduledTask1", '''
			import com.fhoster.livebase.cloudlet.Class1DatabaseDeleteContext;
			import com.fhoster.livebase.cloudlet.Class1DatabaseDeleteTransactionalContext;
			import com.fhoster.livebase.cloudlet.SpiCloudletScheduledTask;
			import com.fhoster.livebase.CloudletScheduledTask;
			import com.fhoster.livebase.cloudlet.CloudletIdGenerator;
			
			
			@CloudletScheduledTask(defaultCronExpression = "1 * * * * *")
			public class MyScheduledTask1 implements SpiCloudletScheduledTask {
			
			    public MyScheduledTask1(CloudletIdGenerator x){
			        
			    }
			    
			  @Override
			  public void run() {
			    // TODO Auto-generated method stub
			
			  }
			
			}
		''')
		var class2 = getSourceFromCode("MyScheduledTask2", '''
			import com.fhoster.livebase.cloudlet.Class1DatabaseDeleteContext;
			import com.fhoster.livebase.cloudlet.Class1DatabaseDeleteTransactionalContext;
			import com.fhoster.livebase.cloudlet.SpiCloudletScheduledTask;
			import com.fhoster.livebase.CloudletScheduledTask;
			import com.fhoster.livebase.cloudlet.CloudletIdGenerator;
			
			@CloudletScheduledTask(defaultCronExpression = "2 * * * * *")
			public class MyScheduledTask2 implements SpiCloudletScheduledTask {
			
			    
			  @Override
			  public void run() {
			    // TODO Auto-generated method stub
			
			  }
			
			}
		''')

		val outputs = sut.processClass(class1, class2)
		val out = outputs.get('MyScheduledTask1.blueprint.xml')
		println(out)

		assertThat(the(out), is(equivalentTo(
			the('''
				<blueprint 
				xmlns:ext="http://aries.apache.org/blueprint/xmlns/blueprint-ext/v1.0.0">
				
				<reference id="idGenerator1"
					interface="com.fhoster.livebase.cloudlet.CloudletIdGenerator"
					ext:proxy-method="classes" />                                
									
				<bean id="bid.myscheduledtask1" class="MyScheduledTask1" scope="singleton">
					<argument ref="idGenerator1"/>
				</bean>
				
				<service ref="bid.myscheduledtask1" id="sid.myscheduledtask1"
					interface="com.fhoster.livebase.cloudlet.SpiCloudletScheduledTask">
					<service-properties>
						<entry key="cronExpression" value="1 * * * * *"/>
						<entry key="taskId" value="bid.myscheduledtask1"/>
					</service-properties>
				</service>
					
				</blueprint>''')
		)));

		val out2 = outputs.get('MyScheduledTask2.blueprint.xml')
		println(out2)
		assertThat(the(out2), is(equivalentTo(
			the('''
			<blueprint 
			xmlns:ext="http://aries.apache.org/blueprint/xmlns/blueprint-ext/v1.0.0">
			
			<bean id="bid.myscheduledtask2" class="MyScheduledTask2" scope="singleton">
			</bean>
			
			<service ref="bid.myscheduledtask2" id="sid.myscheduledtask2"
				interface="com.fhoster.livebase.cloudlet.SpiCloudletScheduledTask">
				<service-properties>
					<entry key="cronExpression" value="2 * * * * *"/>
					<entry key="taskId" value="bid.myscheduledtask2"/>
				</service-properties>
			</service>
				
			</blueprint>''')
		)));
	}
	
	
}