/**
 *   Copyright 2021 Fhoster srl
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase

import org.junit.Test

import static org.hamcrest.CoreMatchers.*
import static org.junit.Assert.*
import static org.xmlmatchers.XmlMatchers.*
import static org.xmlmatchers.transform.XmlConverters.the
import org.junit.Before

class CloudletAuthenticationHandlerTest extends TestProcessor{
	
    CloudletAuthenticationHandlerAnnotationProcessor sut
    
    @Before
    def void setup() {
        sut = new CloudletAuthenticationHandlerAnnotationProcessor(new SequentialUniqueIdGenerator)
    }
    
	@Test
	def void verifyBlueprintGeneration() {

		var class1 = getSourceFromCode("MyAuthenticationHandler", '''
			import com.fhoster.livebase.CloudletAuthenticationHandler;
			import com.fhoster.livebase.cloudlet.__CloudletCurrentUser;
			import com.fhoster.livebase.cloudlet.AuthenticationData;
			import com.fhoster.livebase.cloudlet.SpiCloudletAuthenticationHandler;
			
			@CloudletAuthenticationHandler
			public class MyAuthenticationHandler implements SpiCloudletAuthenticationHandler{
			
				@Override
				public __CloudletCurrentUser authenticate(AuthenticationData data) {
				    return null;
				}
			}
		''')

		val outputs = sut.processClass(class1)
		val out = outputs.get('MyAuthenticationHandler.blueprint.xml')
		println(out)

		assertThat(the(out), is(equivalentTo(
			the('''
				<blueprint 
				xmlns:ext="http://aries.apache.org/blueprint/xmlns/blueprint-ext/v1.0.0">
				
									
				<bean id="bid.myauthenticationhandler" class="MyAuthenticationHandler" scope="singleton">
				</bean>
				
				<service ref="bid.myauthenticationhandler" id="sid.myauthenticationhandler"
					interface="com.fhoster.livebase.cloudlet.SpiCloudletAuthenticationHandler">
					<service-properties>
						<entry key="serviceId" value="service_id_MyAuthenticationHandler" />
					</service-properties>
				</service>
					
				</blueprint>''')
		)));

	}
	
}