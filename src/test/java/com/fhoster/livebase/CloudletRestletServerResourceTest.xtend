/**
 *   Copyright 2021 Fhoster srl
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase

import org.junit.Test

import static org.hamcrest.CoreMatchers.*
import static org.junit.Assert.*
import static org.xmlmatchers.XmlMatchers.*
import static org.xmlmatchers.transform.XmlConverters.the
import org.junit.Before

class CloudletRestletServerResourceTest extends TestProcessor{
	
    CloudletRestletServerResourceAnnotationProcessor sut
    
    @Before
    def void setup() {
        sut = new CloudletRestletServerResourceAnnotationProcessor(new SequentialUniqueIdGenerator)
    }
    
	@Test
	def void verifyBlueprintGeneration() {

		var class1 = getSourceFromCode("MyRestletServerResource", '''
			import com.fhoster.livebase.CloudletRestletServerResource;
			import com.fhoster.livebase.cloudlet.RestletServerResource;
			import com.fhoster.livebase.cloudlet.RestletServerResourceType;
			
			
			@CloudletRestletServerResource
			public class MyRestletServerResource extends RestletServerResource {
				
				@Override
				public String getUrl() {
					return "myurl";
				}
				
				@Override
				public RestletServerResourceType getType() {
					return RestletServerResourceType.EXT;
				}
			}
		''')
		
		val outputs = sut.processClass(class1)
		val out = outputs.get('MyRestletServerResource.blueprint.xml')
		println(out)

		assertThat(the(out), is(equivalentTo(
			the('''
				<blueprint 
				xmlns:ext="http://aries.apache.org/blueprint/xmlns/blueprint-ext/v1.0.0">
				
									
				<bean 
					id="bid.myrestletserverresource" 
					class="MyRestletServerResource"
					scope="singleton">
				</bean>
				
				<service 
					ref="bid.myrestletserverresource" 
					id="sid.myrestletserverresource"
					interface="com.fhoster.livebase.cloudlet.RestletServerResource">
				</service>
					
				</blueprint>''')
		)));

	}

	@Test
	def void verifyErrorOn_nonPublicClass() {

		var class1 = getSourceFromCode("MyRestletServerResource", '''
			import com.fhoster.livebase.CloudletRestletServerResource;
			import com.fhoster.livebase.cloudlet.RestletServerResource;
			import com.fhoster.livebase.cloudlet.RestletServerResourceType;
			
			
			@CloudletRestletServerResource
			class MyRestletServerResource extends RestletServerResource {
				
				@Override
				public String getUrl() {
					return "myurl";
				}
				
				@Override
				public RestletServerResourceType getType() {
					return RestletServerResourceType.EXT;
				}
			}
		''')

		sut.processClassFailure(class1)
	}

}