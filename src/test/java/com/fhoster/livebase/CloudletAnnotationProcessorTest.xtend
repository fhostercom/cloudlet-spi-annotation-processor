/**
 *   Copyright 2021 Fhoster srl
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase

import static org.hamcrest.CoreMatchers.*
import static org.junit.Assert.*
import static org.xmlmatchers.XmlMatchers.*
import static org.xmlmatchers.transform.XmlConverters.the

import org.junit.Test

class CloudletAnnotationProcessorTest extends TestProcessor {
    
    @Test
    def void verifyMailsystemAnnotationManagement() {
    	 var class1 = getSourceFromCode("MyFormActionPlugin", '''
            import com.fhoster.livebase.cloudlet.Class1FormActionContext;
            import com.fhoster.livebase.cloudlet.HandlerException;
            import com.fhoster.livebase.cloudlet.HandlerResult;
            import com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1;
            import com.fhoster.livebase.CloudletEventHandler;
            import com.fhoster.livebase.cloudlet.CloudletMailSystem;
            
            @CloudletEventHandler
            public class MyFormActionPlugin implements SpiClass1FormActionHandlerMyPlugin1 {
            
              public MyFormActionPlugin(CloudletMailSystem mailSystem) {}
              
              @Override
              public HandlerResult doAction(Class1FormActionContext arg0) throws HandlerException {
                return null;
              }
            }
        ''')
        
        val outputs = new CloudletAnnotationProcessor().processClass(class1)
    
	    val out1 = outputs.get('MyFormActionPlugin.blueprint.xml')
	        println(out1)
	        assertThat(the(out1), is(equivalentTo(
	            the('''
	            <blueprint 
	                xmlns:ext="http://aries.apache.org/blueprint/xmlns/blueprint-ext/v1.0.0">
	                
	                <reference id="mailSystem1"
	                    interface="com.fhoster.livebase.cloudlet.CloudletMailSystem"
	                    ext:proxy-method="classes" />                                
	                                    
	                <bean id="bid.myformactionplugin" class="MyFormActionPlugin" scope="singleton">
	                    <argument  ref="mailSystem1"/>
	                </bean>
	                
	            
	                <service ref="bid.myformactionplugin" id="sid.myformactionplugin"
	                    interface="com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1">
	                </service>
	                    
	                </blueprint>''')
	        )));
    	
    }
    
    @Test
    def void verifyCloudletMemberManagerManagement() {
    	 var class1 = getSourceFromCode("MyFormActionPlugin", '''
            import com.fhoster.livebase.cloudlet.Class1FormActionContext;
            import com.fhoster.livebase.cloudlet.HandlerException;
            import com.fhoster.livebase.cloudlet.HandlerResult;
            import com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1;
            import com.fhoster.livebase.CloudletEventHandler;
            import com.fhoster.livebase.cloudlet.CloudletMemberManager;
            
            @CloudletEventHandler
            public class MyFormActionPlugin implements SpiClass1FormActionHandlerMyPlugin1 {
            
              public MyFormActionPlugin(CloudletMemberManager memberManager) {}
              
              @Override
              public HandlerResult doAction(Class1FormActionContext arg0) throws HandlerException {
                return null;
              }
            }
        ''')
        
        val outputs = new CloudletAnnotationProcessor().processClass(class1)
    
	    val out1 = outputs.get('MyFormActionPlugin.blueprint.xml')
	        println(out1)
	        assertThat(the(out1), is(equivalentTo(
	            the('''
	            <blueprint 
	                xmlns:ext="http://aries.apache.org/blueprint/xmlns/blueprint-ext/v1.0.0">
	                
	                <reference id="memberManager1"
	                    interface="com.fhoster.livebase.cloudlet.CloudletMemberManager"
	                    ext:proxy-method="classes" />                                
	                                    
	                <bean id="bid.myformactionplugin" class="MyFormActionPlugin" scope="singleton">
	                    <argument  ref="memberManager1"/>
	                </bean>
	                
	            
	                <service ref="bid.myformactionplugin" id="sid.myformactionplugin"
	                    interface="com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1">
	                </service>
	                    
	                </blueprint>''')
	        )));
    }
    
    @Test
    def void verifyCloudletSessionFactory() {
    	 var class1 = getSourceFromCode("MyFormActionPlugin", '''
            import com.fhoster.livebase.cloudlet.Class1FormActionContext;
            import com.fhoster.livebase.cloudlet.HandlerException;
            import com.fhoster.livebase.cloudlet.HandlerResult;
            import com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1;
            import com.fhoster.livebase.CloudletEventHandler;
            import com.fhoster.livebase.cloudlet.CloudletSessionFactory;
            
            @CloudletEventHandler
            public class MyFormActionPlugin implements SpiClass1FormActionHandlerMyPlugin1 {
            
              public MyFormActionPlugin(CloudletSessionFactory sessionFactory) {}
              
              @Override
              public HandlerResult doAction(Class1FormActionContext arg0) throws HandlerException {
                return null;
              }
            }
        ''')
        
        val outputs = new CloudletAnnotationProcessor().processClass(class1)
    
	    val out1 = outputs.get('MyFormActionPlugin.blueprint.xml')
	        println(out1)
	        assertThat(the(out1), is(equivalentTo(
	            the('''
	            <blueprint 
	                xmlns:ext="http://aries.apache.org/blueprint/xmlns/blueprint-ext/v1.0.0">
	                
	                <reference id="sessionFactory1"
	                    interface="com.fhoster.livebase.cloudlet.CloudletSessionFactory"
	                    ext:proxy-method="classes" />                                
	                                    
	                <bean id="bid.myformactionplugin" class="MyFormActionPlugin" scope="singleton">
	                    <argument  ref="sessionFactory1"/>
	                </bean>
	                
	            
	                <service ref="bid.myformactionplugin" id="sid.myformactionplugin"
	                    interface="com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1">
	                </service>
	                    
	                </blueprint>''')
	        )));
    }
    
    @Test
    def void verifyEntityLockManager() {
    	 var class1 = getSourceFromCode("MyFormActionPlugin", '''
            import com.fhoster.livebase.cloudlet.Class1FormActionContext;
            import com.fhoster.livebase.cloudlet.HandlerException;
            import com.fhoster.livebase.cloudlet.HandlerResult;
            import com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1;
            import com.fhoster.livebase.CloudletEventHandler;
            import com.fhoster.livebase.cloudlet.EntityLockManager;
            
            @CloudletEventHandler
            public class MyFormActionPlugin implements SpiClass1FormActionHandlerMyPlugin1 {
            
              public MyFormActionPlugin(EntityLockManager entityLockManager) {}
              
              @Override
              public HandlerResult doAction(Class1FormActionContext arg0) throws HandlerException {
                return null;
              }
            }
        ''')
        
        val outputs = new CloudletAnnotationProcessor().processClass(class1)
    
	    val out1 = outputs.get('MyFormActionPlugin.blueprint.xml')
	        println(out1)
	        assertThat(the(out1), is(equivalentTo(
	            the('''
	            <blueprint 
	                xmlns:ext="http://aries.apache.org/blueprint/xmlns/blueprint-ext/v1.0.0">
	                
	                <reference id="entityLockManager1"
	                    interface="com.fhoster.livebase.cloudlet.EntityLockManager"
	                    ext:proxy-method="classes" />                                
	                                    
	                <bean id="bid.myformactionplugin" class="MyFormActionPlugin" scope="singleton">
	                    <argument  ref="entityLockManager1"/>
	                </bean>
	                
	            
	                <service ref="bid.myformactionplugin" id="sid.myformactionplugin"
	                    interface="com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1">
	                </service>
	                    
	                </blueprint>''')
	        )));
    }
    
    @Test
    def void verifyEntitySessionFactory() {
    	 var class1 = getSourceFromCode("MyFormActionPlugin", '''
            import com.fhoster.livebase.cloudlet.Class1FormActionContext;
            import com.fhoster.livebase.cloudlet.HandlerException;
            import com.fhoster.livebase.cloudlet.HandlerResult;
            import com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1;
            import com.fhoster.livebase.CloudletEventHandler;
            import com.fhoster.livebase.cloudlet.EntitySessionFactory;
            
            @CloudletEventHandler
            public class MyFormActionPlugin implements SpiClass1FormActionHandlerMyPlugin1 {
            
              public MyFormActionPlugin(EntitySessionFactory sessionFactory) {}
              
              @Override
              public HandlerResult doAction(Class1FormActionContext arg0) throws HandlerException {
                return null;
              }
            }
        ''')
        
        val outputs = new CloudletAnnotationProcessor().processClass(class1)
    
	    val out1 = outputs.get('MyFormActionPlugin.blueprint.xml')
	        println(out1)
	        assertThat(the(out1), is(equivalentTo(
	            the('''
	            <blueprint 
	                xmlns:ext="http://aries.apache.org/blueprint/xmlns/blueprint-ext/v1.0.0">
	                
	                <reference id="entitySessionFactory1"
	                    interface="com.fhoster.livebase.cloudlet.EntitySessionFactory"
	                    ext:proxy-method="classes" />                                
	                                    
	                <bean id="bid.myformactionplugin" class="MyFormActionPlugin" scope="singleton">
	                    <argument  ref="entitySessionFactory1"/>
	                </bean>
	                
	            
	                <service ref="bid.myformactionplugin" id="sid.myformactionplugin"
	                    interface="com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1">
	                </service>
	                    
	                </blueprint>''')
	        )));
    }
    
    @Test
    def void verifyCloudletEntitySessionFactory() {
    	 var class1 = getSourceFromCode("MyFormActionPlugin", '''
            import com.fhoster.livebase.cloudlet.Class1FormActionContext;
            import com.fhoster.livebase.cloudlet.HandlerException;
            import com.fhoster.livebase.cloudlet.HandlerResult;
            import com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1;
            import com.fhoster.livebase.CloudletEventHandler;
            import com.fhoster.livebase.cloudlet.CloudletEntitySessionFactory;
            
            @CloudletEventHandler
            public class MyFormActionPlugin implements SpiClass1FormActionHandlerMyPlugin1 {
            
              public MyFormActionPlugin(CloudletEntitySessionFactory sessionFactory) {}
              
              @Override
              public HandlerResult doAction(Class1FormActionContext arg0) throws HandlerException {
                return null;
              }
            }
        ''')
        
        val outputs = new CloudletAnnotationProcessor().processClass(class1)
    
	    val out1 = outputs.get('MyFormActionPlugin.blueprint.xml')
	        println(out1)
	        assertThat(the(out1), is(equivalentTo(
	            the('''
	            <blueprint 
	                xmlns:ext="http://aries.apache.org/blueprint/xmlns/blueprint-ext/v1.0.0">
	                
	                <reference id="cloudletEntitySessionFactory1"
	                    interface="com.fhoster.livebase.cloudlet.CloudletEntitySessionFactory"
	                    ext:proxy-method="classes" />                                
	                                    
	                <bean id="bid.myformactionplugin" class="MyFormActionPlugin" scope="singleton">
	                    <argument  ref="cloudletEntitySessionFactory1"/>
	                </bean>
	                
	            
	                <service ref="bid.myformactionplugin" id="sid.myformactionplugin"
	                    interface="com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1">
	                </service>
	                    
	                </blueprint>''')
	        )));
    }
    
    @Test
    def void verifyMultipleAnnotationManagement() {

        var class1 = getSourceFromCode("MyFormActionPlugin", '''
            import com.fhoster.livebase.cloudlet.Class1FormActionContext;
            import com.fhoster.livebase.cloudlet.HandlerException;
            import com.fhoster.livebase.cloudlet.HandlerResult;
            import com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1;
            import com.fhoster.livebase.CloudletEventHandler;
            import com.fhoster.livebase.cloudlet.CloudletIdGenerator;
            
            @CloudletEventHandler
            public class MyFormActionPlugin implements SpiClass1FormActionHandlerMyPlugin1 {
            
              public MyFormActionPlugin(CloudletIdGenerator idGen) {}
              
              @Override
              public HandlerResult doAction(Class1FormActionContext arg0) throws HandlerException {
                return null;
              }
            }
        ''')
        var class2 = getSourceFromCode("MyAuthenticationHandler", '''
            import com.fhoster.livebase.CloudletAuthenticationHandler;
            import com.fhoster.livebase.cloudlet.__CloudletCurrentUser;
            import com.fhoster.livebase.cloudlet.AuthenticationData;
            import com.fhoster.livebase.cloudlet.SpiCloudletAuthenticationHandler;
            import com.fhoster.livebase.cloudlet.CloudletIdGenerator;
            
            @CloudletAuthenticationHandler
            public class MyAuthenticationHandler implements SpiCloudletAuthenticationHandler{
            
                public MyAuthenticationHandler(CloudletIdGenerator idGen) {}
                
                @Override
                public __CloudletCurrentUser authenticate(AuthenticationData data) {
                   return null;
                }
            }
        ''')

        val outputs = new CloudletAnnotationProcessor().processClass(class1, class2)

        val out1 = outputs.get('MyFormActionPlugin.blueprint.xml')
        println(out1)
        assertThat(the(out1), is(equivalentTo(
            the('''
            <blueprint 
                xmlns:ext="http://aries.apache.org/blueprint/xmlns/blueprint-ext/v1.0.0">
                
                <reference id="idGenerator1"
                    interface="com.fhoster.livebase.cloudlet.CloudletIdGenerator"
                    ext:proxy-method="classes" />                                
                                    
                <bean id="bid.myformactionplugin" class="MyFormActionPlugin" scope="singleton">
                    <argument  ref="idGenerator1"/>
                </bean>
                
            
                <service ref="bid.myformactionplugin" id="sid.myformactionplugin"
                    interface="com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerMyPlugin1">
                </service>
                    
                </blueprint>''')
        )));

        val out2 = outputs.get('MyAuthenticationHandler.blueprint.xml')
        println(out2)
        assertThat(the(out2), is(equivalentTo(
            the('''
            <blueprint 
                xmlns:ext="http://aries.apache.org/blueprint/xmlns/blueprint-ext/v1.0.0">
                
                <reference id="idGenerator2"
                    interface="com.fhoster.livebase.cloudlet.CloudletIdGenerator"
                    ext:proxy-method="classes" />                                
                                    
                <bean id="bid.myauthenticationhandler" class="MyAuthenticationHandler" scope="singleton">
                    <argument  ref="idGenerator2"/>
                </bean>
                
            
                <service ref="bid.myauthenticationhandler" id="sid.myauthenticationhandler"
                    interface="com.fhoster.livebase.cloudlet.SpiCloudletAuthenticationHandler">
                    <service-properties>
                        <entry key="serviceId" value="service_id_MyAuthenticationHandler" />
                    </service-properties>
                </service>
                    
                </blueprint>''')
        )));
    }
}