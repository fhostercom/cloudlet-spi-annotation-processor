/**
 *   Copyright 2021 Fhoster srl
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase

import org.junit.Test

import static org.hamcrest.CoreMatchers.*
import static org.junit.Assert.*
import static org.xmlmatchers.XmlMatchers.*
import static org.xmlmatchers.transform.XmlConverters.the
import org.junit.Before

class CloudletSMSSenderTest extends TestProcessor{
	
    CloudletSMSSenderAnnotationProcessor sut
    
    @Before
    def void setup() {
        sut = new CloudletSMSSenderAnnotationProcessor(new SequentialUniqueIdGenerator)
    }
    
	@Test
	def void verifyBlueprintGeneration() {

		var class1 = getSourceFromCode("MyCloudletSMSSender", '''
			import com.fhoster.livebase.CloudletSMSSender;
			import com.fhoster.livebase.cloudlet.SpiCloudletSMSSender;
			import com.fhoster.livebase.cloudlet.SMSMessage;
			
			@CloudletSMSSender
			public class MyCloudletSMSSender implements SpiCloudletSMSSender {
			
				@Override
				public void sendSMS(SMSMessage message) {
					// do nothing
				}
			}
		''')

		val outputs = sut.processClass(class1)
		val out = outputs.get('MyCloudletSMSSender.blueprint.xml')
		println(out)

		assertThat(the(out), is(equivalentTo(
			the('''
				<blueprint 
					xmlns:ext="http://aries.apache.org/blueprint/xmlns/blueprint-ext/v1.0.0">
					
										
					<bean id="bid.mycloudletsmssender" class="MyCloudletSMSSender" scope="singleton">
					</bean>
					
				
					<service ref="bid.mycloudletsmssender" id="sid.mycloudletsmssender"
						interface="com.fhoster.livebase.cloudlet.SpiCloudletSMSSender">
					</service>
						
				</blueprint>''')
		)));

	}
	
}