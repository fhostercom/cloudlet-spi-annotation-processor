/**
 *   Copyright 2021 Fhoster srl
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase

import org.junit.Test

import static org.hamcrest.CoreMatchers.*
import static org.junit.Assert.*
import static org.xmlmatchers.XmlMatchers.*
import static org.xmlmatchers.transform.XmlConverters.the
import org.junit.Before

class CloudletLogoutHandlerTest extends TestProcessor{
	
    CloudletLogoutHandlerAnnotationProcessor sut
    
    @Before
    def void setup() {
        sut = new CloudletLogoutHandlerAnnotationProcessor(new SequentialUniqueIdGenerator)
    }
    
	@Test
	def void verifyBlueprintGeneration() {

		var class1 = getSourceFromCode("MyLogoutHandler", '''
			import com.fhoster.livebase.CloudletLogoutHandler;
			import com.fhoster.livebase.cloudlet.LogoutHandlerContext;
			import com.fhoster.livebase.cloudlet.HandlerResult;
			import com.fhoster.livebase.cloudlet.SpiCloudletLogoutHandler;
			
			@CloudletLogoutHandler
			public class MyLogoutHandler implements SpiCloudletLogoutHandler{
			
				@Override
				public HandlerResult logout(LogoutHandlerContext context) {
				    return context.result().none();
				}
			}
		''')

		val outputs = sut.processClass(class1)
		val out = outputs.get('MyLogoutHandler.blueprint.xml')
		println(out)

		assertThat(the(out), is(equivalentTo(
			the('''
				<blueprint 
				xmlns:ext="http://aries.apache.org/blueprint/xmlns/blueprint-ext/v1.0.0">
				
									
				<bean id="bid.mylogouthandler" class="MyLogoutHandler" scope="singleton">
				</bean>
				
				<service ref="bid.mylogouthandler" id="sid.mylogouthandler"
					interface="com.fhoster.livebase.cloudlet.SpiCloudletLogoutHandler">
					<service-properties>
						<entry key="serviceId" value="service_id_MyLogoutHandler" />
					</service-properties>
				</service>
					
				</blueprint>''')
		)));

	}
	
}