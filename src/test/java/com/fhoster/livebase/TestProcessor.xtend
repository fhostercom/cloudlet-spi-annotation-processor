/**
 *   Copyright 2021 Fhoster srl
 * 
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.fhoster.livebase

import com.google.common.base.Charsets
import java.io.File
import java.io.IOException
import java.io.PrintWriter
import java.net.URI
import java.util.Collections
import javax.tools.JavaFileObject
import javax.tools.JavaFileObject.Kind
import javax.tools.SimpleJavaFileObject
import javax.tools.StandardLocation
import javax.tools.ToolProvider

import static org.junit.Assert.*

import static extension com.google.common.io.Files.*
import javax.annotation.processing.AbstractProcessor

class TestProcessor {

	private def processClass0(String out, AbstractProcessor processor, JavaFileObject ... files) {
	    if (processor instanceof AnnotationProcessor)
		  processor.printer = [println(it)]

		val compiler = ToolProvider.systemJavaCompiler

		val libs = #[
			"src/test/resources/osgi.core-5.0.0.jar",
			"src/test/resources/sample-spi.jar",
			"src/test/resources/org.restlet-2.3.9.jar",
			System.getProperty("java.class.path")
		]
		new File(out).mkdirs()
		val options = #[
			"-cp",
			'''«libs.join(":")»''',
			"-s",
			out, // output direcotry for generated resources
			"-d",
			out // output direcotry for compiled test method class
		]
		val task = compiler.getTask(new PrintWriter(System.out), null, null, options, null, files)
		task.setProcessors(#[processor])
		task.call
	}

	protected def processClass(AbstractProcessor processor, JavaFileObject ... files) {
		val out = "build/compiled"
	   	val res = processClass0(out, processor, files);
		assertTrue("Something was broken during input file java compilation", res)

		val output = new File('''«out»/OSGI-INF/blueprint/''').listFiles[f|f.name.endsWith('.xml')].toMap[it.name]// strip away these parts because are problematic with XmlMatchers ..
		.mapValues[f|f.toString(Charsets.UTF_8).replace('xmlns="http://www.osgi.org/xmlns/blueprint/v1.0.0"', '')]

		return output
	}
	
	protected def processClassFailure(AbstractProcessor processor, JavaFileObject ... files) {
		val out = "build/compiled"
	   	val res = processClass0(out, processor, files);
		assertFalse("Should fail but was successful completed", res)
	}	

	/**
	 * Get code sources from a file path such as ../spi-sample/src/main/java 
	 */
	protected def getSourceFromFilePath(String... paths) {
		val compiler = ToolProvider.getSystemJavaCompiler
		val fs = compiler.getStandardFileManager(null, null, null)
		val files = paths.map[new File(it)]
		fs.setLocation(StandardLocation.SOURCE_PATH, files)
		val fileKinds = Collections.singleton(Kind.SOURCE)
		return fs.list(StandardLocation.SOURCE_PATH, "", fileKinds, true)
	}

	protected def getSourceFromCode(String fakeFileName, String code) {
		return new JavaSourceFromString(fakeFileName, code)
	}
}

class JavaSourceFromString extends SimpleJavaFileObject {
	final String code;

	protected new(String name, String code) {
		super(URI.create("string:///" + name.replace('.', '/') + Kind.SOURCE.extension), Kind.SOURCE);
		this.code = code;
	}

	override getCharContent(boolean ignoreEncodingErrors) throws IOException {
		return code
	}

}